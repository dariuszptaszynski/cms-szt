<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$product->name}}</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}?=<?php echo time(); ?>">
</head>
<body>
@include('main_menu')

<div>


<?php
                $photo=explode('/',$product->photo);
                $path='public/storage/storage/';
                ?>
                <img class="rounded mx-auto d-block img-product" src="{{url('')}}<?php echo '/storage/storage/'.$photo[2] ?>" alt="photo">
                <h2 class="text-center">{{$product->name}}</h2>
                <p class="text-justify">{{$product->description}}</p>
</div>

</body>

</html>

