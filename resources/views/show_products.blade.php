<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Produkty</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}?=<?php echo time(); ?>">
</head>

<body>
@include('main_menu')
<h1 class="text-center">Lista produktów</h1>
<div class="products-container">
<div class="products-container-list">
@foreach($products as $product)
<div class="product-box">


<?php
                $photo=explode('/',$product->photo);
                $path='public/storage/storage/';
                ?>
                <div class="product-box-left">
                <img class="rounded mx-auto d-block img-product" src="{{url('')}}<?php echo '/storage/storage/'.$photo[2] ?>" alt="photo">
                </div>
                <div class="product-box-right">
                <h2><a href="{{route('show_product', ['id'=>$product->id])}}">{{$product->name}}</a></h2>
                <p>{{Str::limit($product->description, 100)}}</p>
</div>
</div>
@endforeach
</div>
</div>

</body>

</html>