<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



// Route::group(['namespace'=>'App\Http\Controllers', 'prefix' => 'admin'], function ()
// {
//     Route::get('/', [
//         'uses'=>'AdminController@index',
//         'as'=>'admin_index'
//     ]);

//     Route::get('/add', [
//         'uses'=>'AdminController@add',
//         'as'=>'admin_add'
//     ]);

//     Route::get('/edit', [
//         'uses'=>'AdminController@edit',
//         'as'=>'admin_edit'
//     ]);

//     Route::get('/delete', [
//         'uses'=>'AdminController@delete',
//         'as'=>'admin_delete'
//     ]);
    
    
//     Route::post('/', [
//         'uses'=>'AdminController@index',
//         'as'=>'admin_index'
//         ]);
    
// });


Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home_page');
Route::get('produkt/'.'{id}', 'App\Http\Controllers\HomeController@product')->name('show_product');

Route::get('/contact', 'App\Http\Controllers\ContactUsFormController@createForm')->name('contact_form');

Route::post('/contact', 'App\Http\Controllers\ContactUsFormController@ContactUsForm')->name('contact.store');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('routes', function() {
    \Artisan::call('route:list');
    return '<pre>' . \Artisan::output() . '</pre>';
    });

Route::group([['middleware' => 'auth'], 'prefix'=>'admin_panel','as'=>'admin_panel.'], function(){
    Route::get('/', 'App\Http\Controllers\AdminController@index')->name('panel_admina');
    Route::post('/checklogin', 'App\Http\Controllers\AdminController@checklogin');
    Route::get('/panel', [
                'uses'=>'App\Http\Controllers\AdminController@panel'
                
            ])->name('admin_index');
    
    Route::get('/logout', 'App\Http\Controllers\AdminController@logout');


    Route::get('/add_form', 'App\Http\Controllers\AdminController@add_form')->name('add_form');
    Route::post('/add', 'App\Http\Controllers\AdminController@add')->name('add');

    Route::get('edit_form/'.'{id}', 'App\Http\Controllers\AdminController@edit_form')->name('edit_form');
    Route::post('edit', 'App\Http\Controllers\AdminController@edit')->name('edit');

    Route::get('delete/'.'{id}', 'App\Http\Controllers\AdminController@delete')->name('delete');
    });
