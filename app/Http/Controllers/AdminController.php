<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

use Validator;
use Auth;

class AdminController extends Controller
{
    public function panel(Request $request)
    {
        $data = $request->a;
        if($request->ajax())
        {
        $products = Product::where('name', 'like', '%'.$data.'%')->paginate(10);
        $content = view('products', compact('products'))->render();
        return response()->json(['products'=>$content]);
        }
        else
        {
                $products=Product::paginate(10);
                return view('admin_panel.index', ['products'=>$products]);
        }
    }

    

    public function add_form()
    {
        return view('admin_panel.add');
    }

    public function add(Request $request)
    {
        $product_name=$request->input('product_name');
        $description=$request->input('description');
        $photo=$request->file('photo')->store('public/storage');
        //dd($product_name, $description, $photo);

        $product = new Product();
        $product->name = $product_name;
        $product->description = $description;
        $product->photo = $photo;
        $product->save();

        return redirect()->route('admin_panel.admin_index');
    }

    public function edit_form($id)
    {
        $product = Product::find($id);
        //dd($product);
        return view('admin_panel.edit', ['product'=>$product]);
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');
        $product_name = $request->input('product_name');
        $description = $request->input('description');
        $photo = $request->file('photo');

        if($photo==null)
        {
            Product::where('id', '=', $id)->update(['name'=>$product_name, 'description'=>$description]);
            return redirect()->route('admin_panel.admin_index');
        }
        else
        {
            $photo_path = $request->file('photo')->store('public/storage');
            Product::where('id', '=', $id)->update(['name'=>$product_name, 'description'=>$description, 'photo'=>$photo_path]);
            return redirect()->route('admin_panel.admin_index');
        }
        //dd($product_name, $description, $photo);
    }

    public function delete($id)
    {
        Product::where('id','=', $id)->delete();
        return redirect()->route('admin_panel.admin_index');
    }



    function index()
    {
    return view('admin_panel.login');
    }

    function checklogin(Request $request)
    {
    $this->validate($request, [
    'email' => 'required|email',
    'password' => 'required|min:3'
    ]);
    $user_data = array(
    'email' => $request->get('email'),
    'password' => $request->get('password')
    );
    if(Auth::attempt($user_data))
    {
    return redirect('/admin_panel/panel');
    }
    else
    {
    return back()->with('error', 'Wrong Login Details');
    }
    }

    

    function logout()
    {
    Auth::logout();
    return redirect('admin_panel');
    }
}
