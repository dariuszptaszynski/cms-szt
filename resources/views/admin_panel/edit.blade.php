@if(isset(Auth::user()->email))

@extends('admin_layout.layout')
@section('content')

<form action="{{route('admin_panel.edit')}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
<input type="hidden" value="{{$product->id}}" name="id">
Nazwa produktu:<br />
<input type="text" name="product_name" class="product_name" value="{{$product->name}}"><br />
Opis<br />
<textarea name="description" class="description">{{$product->description}}</textarea><br />
Zdjęcie <br />
<?php
    $photo=explode('/',$product->photo);
?>
<img src="{{url('')}}<?php echo '/storage/storage/'.$photo[2] ?>" alt="photo" width="50px"><br />
<input type="file" name="photo" class="photo"><br /><br />
<input type="submit">
</form>
<a href="{{route('admin_panel.admin_index')}}">Anuluj</a>

@endsection
@else
<script>window.location = "/admin_panel";</script>
   @endif

