<html>
    <head>
        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="{{ url('/css/admin/style.css') }}?v=<?php echo time()?>">
    </head>
    <body class="b">
        <div class="wrapper">
            <header>
            @include('admin_layout.includes.menu')
            </header>

            @yield('content')

            <footer>
                @include('admin_layout.includes.footer')
            </footer>
        </div>
    </body>
</html>