<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $product = Product::get();

        return view('show_products', ['products'=>$product]);
    }

    public function product($id)
    {
        $product = Product::find($id);
        //dd($products);
        return view('product_view', ['product'=>$product]);
    }
}
