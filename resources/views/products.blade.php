<table class="table-products">
        <tr>
            <td class="td-id text-center">Id</td>
            <td class="td-product-name text-center">Nazwa produktu</td>
            <td class="td-description text-center">Opis</td>
            <td class="td-photo text-center">Zdjęcie</td>
            <td colspan="3" class="td-actions text-center">Akcje</td>
            
        </tr>

@foreach($products as $product)
    @if($product->id!='')
        <tr>
            <td>
                {{$product->id}}
            </td>
            <td>
                {{$product->name}}
            </td>
            <td>
                {{ \Illuminate\Support\Str::limit($product->description, 50, $end='...') }}
            </td>
            <td>
                <?php
                $photo=explode('/',$product->photo);
                $path='public/storage/storage/';
                ?>
                <img src="{{url('')}}<?php echo '/storage/storage/'.$photo[2] ?>" alt="photo" height="50px">
            </td>
            <td>
                <a class="show-btn" href="{{route('show_product', ['id'=>$product->id])}}">Podgląd</a>
            </td>
            <td>
                <a class="edit-btn" href="{{route('admin_panel.edit_form', ['id'=>$product->id])}}">Edytuj</a>
            </td>
            <td>
                <a class="delete-btn" href="{{route('admin_panel.delete', ['id'=>$product->id])}}">Usuń</a>
            </td>
        </tr>
        @else
        "Brak produktów"
        @endif
    @endforeach
    
    </table>

<div class="pagination-container">
    {{ $products->links() }}
    </div>

    