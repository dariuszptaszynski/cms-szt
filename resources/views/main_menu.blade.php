<div class="menu-container">
        <div class="menu-container-align">
            <ul class="main-menu">
                <li><a href="{{route('home_page')}}">Strona główna</a></li>
                <li><a href="{{route('contact_form')}}">Kontakt</a></li>
                <li><a target="blank" href="{{route('admin_panel.panel_admina')}}">Panel administratora</a></li>
            </ul>
        </div>
    </div>