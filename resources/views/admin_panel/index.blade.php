@if(isset(Auth::user()->email))

@extends('admin_layout.layout')
@section('content')


<a class="add-btn" href="{{route('admin_panel.add_form')}}">Dodaj</a>

<div style="width:100%;display:flex; justify-content: flex-end;">
<form method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="text" id="search_field" name="search_field" class="search_field" placeholder="Szukaj">
</form>
</div>


<div class="products-container">

        
    @include('products')


</div>






<!-- <a href="{{ url('/admin_panel/logout') }}">Logout</a> -->

<script>
$(document).ready(function(){
$(".search_field").keyup(function(){
let a=$(this).val();
//alert(a);
var url = "{{ route('admin_panel.admin_index')}}";
var token = "{{ csrf_token()}}";
let productsContainer = document.querySelector('.products-container');
console.log(productsContainer);
$.ajax({
type: "GET",
url: url,
 data: {a:a,_token:token},
dataType: 'json',
success: function(data)
{
console.log(a);
productsContainer.innerHTML = data.products;
//console.log(data.products);
},
error:function(){
alert(a);
}
})
});
});
</script>




@endsection
@else
<script>window.location = "/admin_panel";</script>
   @endif